Maven, Google Guice, Hibernate Validator, AspectJ, Google Guava, Apache Commons, SLF4J, Log4J, Pitest, 
JUnit, 
AssertJ, 
Mockito, 
Twitter Bootstrap.

Master branch on **GitHub**: https://github.com/VoronaPavel/travel-agency

Brief architectural overview **(PDF)**: https://www.lucidchart.com/publicSegments/view/5598f537-cb70-4f0f-b8c2-1e3f0a009030/image.pdf